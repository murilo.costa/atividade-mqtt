# Informações pessoais

Aluno: Murilo Gustavo Nabarrete Costa - Aluno especial

Formação: Engenharia de Software - UFMS 2020

# Link para a biblioteca Mtqq

https://www.npmjs.com/package/mqtt

# Execução da aplicação

Realize o npm install e instancie um broker que opere via localhost com conexão via WS na porta 9001, caso deseje mudar basta alterar a URL e o protocolo nos arquivos clientSubscriber.js e clientPublisher.js

Após tudo instalado e instanciado utilize o comando npm start e aguarde a abertura da aplicação!