import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import clientSubscriber from './clientSubcriber';
import clientPublisher from './clientPublisher';

function App() {
  const [created, setCreated] = useState(0);
  const [started, setStarted] = useState(false);
  const [clientPublisherObj, setClientPublisherObj] = useState({});
  const [clientSubscriberObj, setClientSubscriberObj] = useState({});
  const [commands, setCommands] = useState('');
  const [logPublish, setLogPublish] = useState('');

  useEffect(() => {
    const publisher = clientPublisher.createClient();
    setClientPublisherObj(publisher);

    const subscriber = clientSubscriber.createClient();
    clientSubscriber.listening(subscriber, (topic, message) => {
      const data = JSON.parse(message.toString());
      setLogPublish(logPublish => `${message.toString()}\n\n${logPublish}`);

      data.rooms.forEach((room, index) => {
        let command = [];
        if(room.temperature < 18 ){
           command.push(`The temperature of room ${index + 1} is below 18ºc`);
        }else if(room.temperature > 27){
          command.push(`The temperature of room ${index + 1} is over 27ºc`);
        }
        if(room.humidity < 40 ){
          command.push(`The humidity of room ${index + 1} is below 40%`);
        }else if(room.humidity > 55){
          command.push(`The humidity of room ${index + 1} is over 55%`);
        }
        if (Array.isArray(command) && command.length) {
          setCommands(commands => `${command.join(" | ")}\n${commands}`);
        }
      });
    });

    setClientSubscriberObj(subscriber);

    console.log('Publisher: ',publisher);
    console.log('Subscriber: ',subscriber);
  },[created]);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Assignment 5: Lab MQTT
        </p>
        {!started ? 
        <a className="myButton" onClick={ () => {
          clientPublisher.startPublish(clientPublisherObj);
          setStarted(true);
          setCommands('');
          setLogPublish('');
        }}>
          Publish temperature and humidity (5 sec delay)
        </a>: ''}

        {started ? 
        <a className="myButton" onClick={ () => {
          clientPublisher.stopPublish(clientPublisherObj);
          clientSubscriber.close(clientSubscriberObj);
          setStarted(false);
          setCreated(created => created + 1);
        }}>Stop Publish
        </a>: ''}
        <p>
          Commands:
        </p>
        <textarea id="story" name="story" rows="10" cols="100" value= {commands} readOnly={true}>
        </textarea>
        <p>
          Log Publish
        </p>
        <textarea id="story" name="story" rows="10" cols="100" value= {logPublish} readOnly={true}>
        </textarea>
      </header>
    </div>
  );
}

export default App;
