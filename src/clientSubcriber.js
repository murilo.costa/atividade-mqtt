import mqtt from 'mqtt';

const clientSubscriber = {
    createClient: () => {
        const client =  mqtt.connect('mtqq://127.0.0.1:9001', [{ host: 'localhost', port: 9001},]);
        client.on('connect', function () {
            client.subscribe('presence', function (err) {
                if(err){
                    console.log(err);
                }
            });
        });
        return client;
    },
    listening: (client, callback) => {
        client.on('message', callback);
    },
    close: (client) => {
        client.end();
    }
} 

export default clientSubscriber;
