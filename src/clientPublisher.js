import mqtt from 'mqtt';

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function makeData(){
    return {
        rooms: 
        [
        {temperature: getRandomInt(15,30), humidity: getRandomInt(40,60)}, 
        {temperature: getRandomInt(15,30), humidity: getRandomInt(40,60)},
        {temperature: getRandomInt(15,30), humidity: getRandomInt(40,60)},
        ]
    };
}

function LoopPublish(client) {       
    setTimeout(function() {
        if (client.connected) {
            client.publish('presence', JSON.stringify(makeData()));
            LoopPublish(client); 
        }
    }, 5000)
}

const clientPublisher = {
    createClient: () => {
        return mqtt.connect('mtqq://127.0.0.1:9001', [{ host: 'localhost', port: 9001},]);
    },
    startPublish: (client) => {
       LoopPublish(client);
    },
    stopPublish: (client) => {
        client.end();
    }
} 

export default clientPublisher;
